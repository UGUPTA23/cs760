import math
import matplotlib.pyplot as plt
import random
import numpy as np
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
import scipy as sp


class Node:
    def __init__(self , label=None, feature=None, threshold=None, lChild=None, rChild=None, numNodes=1)-> None:
        self.label = label
        self.feature = feature
        self.threshold = threshold
        self.lChild = lChild
        self.rChild = rChild
        self.numNodes = numNodes


def candidateSplits(data):
    c = set()
    # for each feature
    for i in range(0, len(data[0])-1):
        # sort data on feature i
        sortedData = sorted(data, key=lambda x: x[i])

        # iterate over sorted data to find splits based on where label changes
        for j in range(1, len(sortedData)):
            # check for split
            if sortedData[j][-1] != sortedData[j-1][-1]:
                c.add((i,sortedData[j][i]))
    return c

def calLabelEntropy(data):
    labelCount = {}
    total = len(data)
    for i in range(0,total):
        label = data[i][-1]
        if label in labelCount:
            labelCount[label] += 1
        else:
            labelCount[label] = 1
    result = 0
    for label in labelCount:
        p = labelCount[label]/total
        result -= p*math.log2(p)
    return result, labelCount


def calSplitEntropy(lArray, rArray):
    total = len(lArray) + len(rArray)
    p1 = len(lArray)/total
    p2 = len(rArray)/total
    if p1 == 0 or p2 == 0:
        return 0
    return (-1)*p1*math.log2(p1) - p2*math.log2(p2)


def calGain(labelEnt, lArray, rArray):
    # calculate conditional entropy of label given split
    total = len(lArray) + len(rArray)
    ent1, _ = calLabelEntropy(lArray)
    p1 = len(lArray)/total
    
    ent2, _ = calLabelEntropy(rArray)
    p2 = len(rArray)/total
    
    condLabelEnt = p1*ent1 + p2*ent2
    return labelEnt - condLabelEnt


def evalSplits(data, C, labelEnt):
    bestSplit = None
    bestGainRatio = 0
    lTree = None
    rTree = None
    for split in C:
        featureIndex = split[0]
        featureVal = split[1]
        #sort data on feature index
        sortedData = sorted(data, key=lambda x: x[featureIndex])
        featureValIndex = 0
        for i in range(0,len(sortedData)):
            if sortedData[i][featureIndex] == featureVal:
                featureValIndex = i
                break
        lArray = sortedData[0:featureValIndex]
        rArray = sortedData[featureValIndex:]

        #calculate split entropy
        splitEntropy = calSplitEntropy(lArray, rArray)

        if splitEntropy == 0:
            continue

        # print(split)
        # print(labelEnt)

        #calculate info gain
        infoGain = calGain(labelEnt, lArray, rArray)
        # print(infoGain)

        #calculate gain ratio
        gainRatio = infoGain/splitEntropy

        #compare
        if gainRatio > bestGainRatio:
            bestGainRatio = gainRatio
            bestSplit = split
            lTree = rArray
            rTree = lArray
    return bestSplit, lTree, rTree


def makeDT(data):
    labelEnt, labelCount = calLabelEntropy(data)
    # calculate candidate splits
    c = candidateSplits(data)
    # print(c)

    # evaluate splits
    bestSplit, lTree, rTree = evalSplits(data, c, labelEnt)

    # if stopping criteria
    if bestSplit is None:
        y = -1
        if data is None or len(data) == 0:
            y = 1
        else:
            numLabel1 = labelCount[1] if 1 in labelCount else 0
            numLabel0 = labelCount[0] if 0 in labelCount else 0
            y = 1 if numLabel1 >= numLabel0 else 0
        return Node(label=y)
    else:
        lChild = makeDT(lTree)
        rChild = makeDT(rTree)
        internalNode = Node(feature=bestSplit[0], threshold=bestSplit[1], lChild=lChild, rChild=rChild, numNodes=1+lChild.numNodes+rChild.numNodes)
        return internalNode


def predict(data, node):
    if node.label is not None:
        return float(node.label)
    else:
        if (data[node.feature] >= node.threshold):
            return predict(data, node.lChild)
        else:
            return predict(data, node.rChild)


def readFile(filename):
    data = []
    with open(filename, 'r') as file:
        for line in file:
            nums = line.strip().split()
            subarray = [float(num) for num in nums]
            data.append(subarray)
    return data

def printTree(root):
    if (root.label is not None):
        print(f"Leaf Node with Label {root.label}")
    else:
        print(f"Internal Node with X{int(root.feature + 1)} >= {root.threshold}. Children are:")
        print("left:")
        printTree(root.lChild)
        print("right:")
        printTree(root.rChild)
        print("-----------------")
    return

def plotData(data, name='plot.png', model=None):
    if model is None:
        model = makeDT(data)

    X = [item[0:2] for item in data]
    y = [item[-1] for item in data]
    X = np.array(X)
    y = np.array(y)

    x_min, x_max = X[:, 0].min() - 0.1, X[:, 0].max() + 0.1
    y_min, y_max = X[:, 1].min() - 0.1, X[:, 1].max() + 0.1

    xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.01), np.arange(y_min, y_max, 0.01))

    meshData = np.c_[xx.ravel(), yy.ravel()]
    Z = []
    for item in meshData:
        prediction = predict(item, model)
        Z.append(prediction)
    Z = np.array(Z)
    Z = Z.reshape(xx.shape)

    plt.figure(figsize=(10,10))
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.RdBu, s=20)

    # Create a contour plot to visualize the decision boundary
    plt.contourf(xx, yy, Z, alpha=0.4, cmap=plt.cm.RdBu)

    plt.xlabel('X1')
    plt.ylabel('X2')
    plt.title('Decision Boundary Visualization')
    # plt.show()

    
    # for item in data:
    #     plt.scatter(item[0], item[1], c='r' if item[2] else 'b')
    # plt.xlabel("X1")
    # plt.ylabel("X2")
    # plt.grid(True)
    plt.savefig(name)


def calcError(model, testData):
    numIncorrect =  0
    for item in testData:
        label = predict(item[0:2], model)
        if (label != item[-1]):
            numIncorrect += 1
    return numIncorrect/len(testData)


def splitDBig(data):
    random.shuffle(data)
    dtest = data[8192:]
    Xtest = [item[0:2] for item in dtest]
    ytest = [item[-1] for item in dtest]
    Xtest = np.array(Xtest)
    ytest = np.array(ytest)
    
    d8192 = data[0:8192]
    
    sk8192 = DecisionTreeClassifier()
    X = [item[0:2] for item in d8192]
    y = [item[-1] for item in d8192]
    X = np.array(X)
    y = np.array(y)
    sk8192.fit(X,y)
    ypred = sk8192.predict(Xtest)
    err = 1 - accuracy_score(ytest, ypred)
    print(f"8192 & {sk8192.tree_.node_count} & {err}")
    # m8192 = makeDT(d8192)
    # err8192 = calcError(m8192, dtest)
    # print(f"8192 & {m8192.numNodes} & {err8192}")
    # plotData(d8192, '8192.png', m8192)

    d2048 = d8192[0:2048]
    sk2048 = DecisionTreeClassifier()
    X = [item[0:2] for item in d2048]
    y = [item[-1] for item in d2048]
    X = np.array(X)
    y = np.array(y)
    sk2048.fit(X,y)
    ypred = sk2048.predict(Xtest)
    err = 1 - accuracy_score(ytest, ypred)
    print(f"2048 & {sk2048.tree_.node_count} & {err}")
    # m2048 = makeDT(d2048)
    # err2048 = calcError(m2048, dtest)
    # print(f"2048 & {m2048.numNodes} & {err2048}")
    # plotData(d2048, '2048.png', m2048)

    d512 = d2048[0:512]
    sk512 = DecisionTreeClassifier()
    X = [item[0:2] for item in d512]
    y = [item[-1] for item in d512]
    X = np.array(X)
    y = np.array(y)
    sk512.fit(X,y)
    ypred = sk512.predict(Xtest)
    err = 1 - accuracy_score(ytest, ypred)
    print(f"512 & {sk512.tree_.node_count} & {err}")
    # m512 = makeDT(d512)
    # err512 = calcError(m512, dtest)
    # print(f"512 & {m512.numNodes} & {err512}")
    # plotData(d512, '512.png', m512)

    d128 = d512[0:128]
    sk128 = DecisionTreeClassifier()
    X = [item[0:2] for item in d128]
    y = [item[-1] for item in d128]
    X = np.array(X)
    y = np.array(y)
    sk128.fit(X,y)
    ypred = sk128.predict(Xtest)
    err = 1 - accuracy_score(ytest, ypred)
    print(f"128 & {sk128.tree_.node_count} & {err}")
    # m128 = makeDT(d128)
    # err128 = calcError(m128, dtest)
    # print(f"128 & {m128.numNodes} & {err128}")
    # plotData(d128, '128.png', m128)

    d32 = d128[0:32]
    sk32 = DecisionTreeClassifier()
    X = [item[0:2] for item in d32]
    y = [item[-1] for item in d32]
    X = np.array(X)
    y = np.array(y)
    sk32.fit(X,y)
    ypred = sk32.predict(Xtest)
    err = 1 - accuracy_score(ytest, ypred)
    print(f"32 & {sk32.tree_.node_count} & {err}")
    # m32 = makeDT(d32)
    # err32 = calcError(m32, dtest)
    # print(f"32 & {m32.numNodes} & {err32}")
    # plotData(d32, '32.png', m32)


def genLagData(a,b):
    # train
    X = np.random.uniform(a, b, 100)
    y = np.sin(X)
    f = sp.interpolate.lagrange(X,y)

    # generate test data
    XTest = np.random.uniform(a, b, 10)
    yTest = np.sin(XTest)

    # calculate train and test error
    yTrainPred = f(X)
    trainErr = np.mean((yTrainPred - y)**2)
    yTestPred = f(XTest)
    testErr = np.mean((yTestPred-yTest)**2)

    print(f"Train Error: {trainErr}; Test Error: {testErr}")

    # x_plot = np.linspace(a, b, 1000)
    # y_plot = np.sin(x_plot)

    # plt.figure(figsize=(10, 5))
    # plt.ylim(-5,5)
    # plt.plot(x_plot, y_plot, label="True Function", linestyle='--', linewidth=2)
    # plt.scatter(X, y, label="Training Points", color='red')
    # plt.plot(x_plot, f(x_plot), label="Interpolated Function", color='green', linewidth=2)
    # plt.xlabel('x')
    # plt.ylabel('y')
    # plt.legend()
    # plt.title('Lagrange Interpolation')
    # plt.show()

    std_devs = [0, 1, 2, 5, 10, 25]

    # adding noise
    for std_dev in std_devs:
        # Generate Gaussian noise with a mean of zero
        noise = np.random.normal(loc=0, scale=std_dev, size=len(X))
        XNoisy = X + noise
        yNoisy = np.sin(XNoisy)
        model = sp.interpolate.lagrange(XNoisy, yNoisy)

        yTrainNoisyPred = model(XNoisy)
        trainErr = np.mean((yTrainNoisyPred - yNoisy)**2)
        yTestPred = model(XTest)
        testErr = np.mean((yTestPred-yTest)**2)

        print(f"Train Error: {math.log(trainErr)}; Test Error: {math.log(testErr)}")
    # x_plot = np.linspace(a, b, 1000)
    # y_plot = np.sin(x_plot)

    # plt.figure(figsize=(10, 5))
    # plt.ylim(-2,2)
    # plt.xlim(a,b)
    # plt.plot(x_plot, y_plot, label="True Function", linestyle='--', linewidth=2)
    # plt.scatter(XNoisy, yNoisy, label="Training Points", color='red')
    # plt.plot(x_plot, model(x_plot), label="Interpolated Function", color='green', linewidth=2)
    # plt.xlabel('x')
    # plt.ylabel('y')
    # plt.legend()
    # plt.title('Lagrange Interpolation')
    # plt.show()



# genLagData(0,10.0)
data = readFile("D1.txt")
# splitDBig(data)
# plotData(data)
root = makeDT(data)

printTree(root)


