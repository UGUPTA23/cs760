import numpy as np
from scipy.stats import multivariate_normal
from sklearn.datasets import make_spd_matrix
import matplotlib.pyplot as plt

SEED = 4
np.random.seed(SEED)

def genData(sig):
    m1 = np.array([-1,-1])
    cov1 = sig*np.array([[2, 0.5],[0.5,1]])

    m2 = np.array([1,-1])
    cov2 = sig*np.array([[1,-0.5],[-0.5,2]])

    m3 = np.array([0,1])
    cov3 = sig*np.array([[1,0],[0,2]])

    n = 100

    d1 = np.random.multivariate_normal(m1, cov1, n)
    d2 = np.random.multivariate_normal(m2, cov2, n)
    d3 = np.random.multivariate_normal(m3, cov3, n)

    data = np.vstack((d1, d2, d3))

    return data

DATA = []
sigmas = [0.5, 1, 2, 4, 8]
for sig in sigmas:
    DATA.append(genData(sig))

def assign(C,data,k):
    assignments = np.zeros(len(data))
    distances = np.zeros(len(data))
    clusterMap = {}
    for i in range(k):
        clusterMap[i] = []
    for i in range(len(data)):
        point = data[i]
        minD = float('inf')
        for j in range(k):
            d = np.linalg.norm(point - C[j])
            if d<minD:
                minD = d
                assignments[i] = j
        clusterMap[assignments[i]].append(point)
        distances[i] = minD
    return clusterMap, assignments, distances

def updateCenters(clusterMap,k):
    Cnew = []
    for i in range(k):
        points = np.array(clusterMap[i])
        sum = np.sum(points, axis=0)
        cnew = sum/len(points)
        Cnew.append(cnew.tolist())
    return np.array(Cnew)

def accuracykMeans(assignments, C):
    means = [[-1,-1],[1,-1],[0,1]]
    means = np.array(means)

    # implemented using chatGPT
    # Calculate pairwise Euclidean distances between points in A and B
    distances = np.sqrt(np.sum((means[:, np.newaxis] - C) ** 2, axis=2))
    # Find the index of the closest point in A for each point in B
    closest_points_indices = np.argmin(distances, axis=0)

    numCorrect = 0
    for i in range(len(assignments)):
        assignment = int(assignments[i])
        actual = int(i/100)
        predicted = closest_points_indices[assignment]
        if actual == predicted:
            numCorrect += 1
    return numCorrect/len(assignments)

def kMeans(k=3):
    for dset in range(len(sigmas)):
        data = DATA[dset]
        # print(data.shape)

        # initialize cluster centers
        r = np.random.randint(0, len(data) - 1)
        C = [data[r]]
        for i in range(1,k):
            dist = []
            for point in data:
                minD = float('inf')
                for center in C:
                    d = np.linalg.norm(point - center)
                    minD = min(minD,d)
                dist.append(minD)
            dist = np.array(dist)
            distSquared = dist**2
            total = np.sum(distSquared)
            probabilities = distSquared/total
            flatData = data.reshape(-1,2)
            randIndex = np.random.choice(len(flatData), p=probabilities)
            c = data[randIndex]
            C.append(c)
        C = np.array(C)
        
        # first iteration of assignment, update
        assignedClusterMap, assignments, _ = assign(C,data,k)
        Cnew = updateCenters(assignedClusterMap,k)

        # repeat until convergence
        while(not np.array_equal(np.sort(C), np.sort(Cnew))):
            C = Cnew
            assignedClusterMap, assignments, distances = assign(C,data,k)
            Cnew = updateCenters(assignedClusterMap,k)

        distSquared = distances**2
        obj = np.sum(distSquared)
        acc = accuracykMeans(assignments, Cnew)
        print(f"acc: {acc}")
        print(f"obj: {obj}")
            
# kMeans()


def E(k, priors, means, covs, data, n):
    assignmentProb = np.zeros((k,n))
    gaussians = []
    for i in range(k):
        gaussians.append(multivariate_normal(mean=means[i], cov=covs[i]))

    # compute posteriors & neg log likelihood
    nll = 0.0
    for j in range(n):
        priorLhood = np.zeros(k)
        for i in range(k):
            gaussian = gaussians[i]
            lhood = gaussian.pdf(data[j])
            priorLhood[i] = lhood*priors[i]
        
        sumPL = np.sum(priorLhood)
        for i in range(k):
            assignmentProb[i][j] = priorLhood[i]/sumPL

        nll -= np.log(sumPL)
    
    return assignmentProb, nll

def M(weights, k, n, data):
    priors = []
    means = []
    covs = []

    for i in range(k):
        # update priors
        sumWeights = np.sum(weights[i])
        priors.append(sumWeights/n)

        # update means
        weightedSum = np.dot(weights[i], data)
        means.append(weightedSum/sumWeights)
        
        # update covariance
        covNum = np.zeros((2,2))
        for j in range(n):
            point = data[j]
            d = point-means[i]
            p = np.outer(d,d)
            covNum += weights[i][j]*p
        cov = covNum/sumWeights
        covs.append(cov)
    
    return priors, means, covs

def accuracyGMM(weights, means, n):
    actualMeans = [[-1,-1],[1,-1],[0,1]]
    actualMeans = np.array(actualMeans)

    # implemented using chatGPT
    # Calculate pairwise Euclidean distances between points in A and B
    distances = np.sqrt(np.sum((actualMeans[:, np.newaxis] - means) ** 2, axis=2))
    # Find the index of the closest point in A for each point in B
    closest_points_indices = np.argmin(distances, axis=0)

    numCorrect = 0
    for i in range(n):
        assignment = np.argmax(weights[:, i])
        actual = int(i/100)
        predicted = closest_points_indices[assignment]
        if actual == predicted:
            numCorrect += 1
    return numCorrect/n

print("-----------------------------------------------")

def GMM():
    k = 3
    for dset in range(len(sigmas)):
        data = DATA[dset]
        n = len(data)
        # initialize z priors
        priors = np.random.dirichlet(np.ones(k))
        # a = 1/3
        # priors = [a for i in range(3)]

        # initialize means and covariances
        means = []
        covs = []
        for i in range (k):
            r = np.random.randint(0, n-1)
            means.append(data[r])
            cov = make_spd_matrix(2, random_state=SEED+i)
            covs.append(cov)

        # print(f"init means: {means}")
        # print(f"init covs: {covs}")

        prevLoss = float('inf')
        deltaLoss = 1
        # iterate EM
        while(deltaLoss > 0.01):
            weights, loss = E(k, priors, means, covs, data, n)
            priors, means, covs = M(weights, k, n, data)
            # print(loss)
            deltaLoss = prevLoss - loss
            prevLoss = loss

        # print(weights[0][0])
        # print(weights[1][0])
        # print(weights[2][0])
        # print(np.argmax(weights[:, 0]))

        # calculate accuracy
        acc = accuracyGMM(weights, means, n)
        
        print(f"acc: {acc}")
        print(f"loss: {loss}")
        # print(f"final means: {means}")
        # print(f"final covs: {covs}")

# GMM()


def buggyPCA(data, d):
    U, S, Vt = np.linalg.svd(data, full_matrices=False)
    pc = Vt.T[:, :d]
    Z = np.dot(data,pc)
    recon = np.dot(Z, pc.T)

    return Z, pc, recon

def demeanedPCA(data, d):
    mean = np.mean(data, axis=0)
    cData = data - mean
    U, S, Vt = np.linalg.svd(cData, full_matrices=False)
    pc = Vt.T[:, :d]
    Z = np.dot(cData, pc)
    recon = np.dot(Z, pc.T) + mean

    return Z, pc, recon

def normalizedPCA(data, d):
    mean = np.mean(data, axis=0)
    sdev = np.std(data, axis=0)
    normData = (data - mean)/sdev
    U, S, Vt = np.linalg.svd(normData, full_matrices=False)
    pc = Vt.T[:, :d]
    Z = np.dot(normData, pc)
    recon = np.dot(Z, pc.T)*sdev + mean

    return Z, pc, recon

def DRO(data, d):
    b = np.mean(data, axis=0)
    Y = data - b
    U, S, Vt = np.linalg.svd(Y, full_matrices=False)
    A = Vt.T[:, :d]
    Z = np.dot(Y,A)
    recon = np.dot(Z, Vt[:d]) + b

    return Z, A, recon
    
def reconErr(data, recon):
    return np.sum((data - recon)**2)/len(data)

def plotData(data, recon, title):
    plt.figure()
    plt.scatter(data[:, 0], data[:, 1], marker='.', color='r', label='original data')
    plt.scatter(recon[:, 0], recon[:, 1], marker='x', color='b', label='reconstructed data')
    plt.xlim(0, 10)
    plt.ylim(0, 10)
    plt.legend()
    plt.title(title)
    plt.savefig(f"{title}.png")

def plot_knee(data1000D):
    cData1000 = data1000D - np.mean(data1000D, axis=0)
    U, S, Vt = np.linalg.svd(cData1000, full_matrices=False)
    plt.figure()
    plt.plot(S, marker='.')
    plt.axvline(x = 30, color = 'r', linestyle=':', label='Knee Point=30')
    plt.title('Singular Values of data1000D')
    plt.xlabel('Index')
    plt.ylabel('Singular Values')
    plt.grid(True)
    plt.legend()
    plt.savefig("knee.png")

data2D = np.loadtxt("./data/data2D.csv", delimiter=',')
data1000D = np.loadtxt("./data/data1000D.csv", delimiter=',')

plot_knee(data1000D)

ZB, pcB, reconB = buggyPCA(data2D, 1)
plotData(data2D, reconB, "Buggy_PCA")

ZD, pcD, reconD = demeanedPCA(data2D, 1)
plotData(data2D, reconD, "Demeaned_PCA")

ZN, pcN, reconN = normalizedPCA(data2D, 1)
plotData(data2D, reconN, "Normalized_PCA")

ZDRO, pcDRO, reconDRO = DRO(data2D, 1)
plotData(data2D, reconDRO, "DRO")

print(f"Reconstruction Error for data2D Buggy: {reconErr(data2D, reconB)}")
print(f"Reconstruction Error for data2D Demeaned: {reconErr(data2D, reconD)}")
print(f"Reconstruction Error for data2D Normalized: {reconErr(data2D, reconN)}")
print(f"Reconstruction Error for data2D DRO: {reconErr(data2D, reconDRO)}")

ZB, pcB, reconB = buggyPCA(data1000D, 30)
ZD, pcD, reconD = demeanedPCA(data1000D, 30)
ZN, pcN, reconN = normalizedPCA(data1000D, 30)
ZDRO, pcDRO, reconDRO = DRO(data1000D, 30)

print(f"Reconstruction Error for data1000D Buggy: {reconErr(data1000D, reconB)}")
print(f"Reconstruction Error for data1000D Demeaned: {reconErr(data1000D, reconD)}")
print(f"Reconstruction Error for data1000D Normalized: {reconErr(data1000D, reconN)}")
print(f"Reconstruction Error for data1000D DRO: {reconErr(data1000D, reconDRO)}")