import numpy as np
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.preprocessing import LabelBinarizer

import torch
import torchvision
from torchvision import datasets, transforms


SMOOTH = 0.5
PRIOR = 1/3

# generate bag of characters
def genBag():
    dir = "languageID"
    y = np.empty(60, dtype='U1')
    labelSpace = ['e', 'j', 's']
    X = []
    letterMap = {chr(97 + i): i for i in range(26)}
    letterMap[' '] = 26
    for i in range(len(labelSpace)):
        for j in range(20):
            x = np.zeros(27)
            filename = f"{dir}/{labelSpace[i]}{j}.txt"
            y[j+i*20] = labelSpace[i]
            
            with open(filename) as file:
                while True:
                    char = file.read(1)
                    char = char.lower()
                    if not char:
                        break
                    if char not in letterMap:
                        continue
                    x[letterMap[char]] += 1
            X.append(x)
    return y,np.array(X)

Y,X = genBag()

indices = np.concatenate((np.arange(10), np.arange(20, 30), np.arange(40, 50)))
Ytrain = Y[indices]
Xtrain = X[indices]

def calcLikelihood(XTrain):
    L = []
    for i in range(3):
        totalChars = 27*SMOOTH
        cumChars = np.full(27, SMOOTH)
        for j in range(10):
            dataPoint = XTrain[i*10+j]
            for k in range(len(dataPoint)):
                cumChars[k] += dataPoint[k]
                totalChars += dataPoint[k]
        logLikelihood = np.log10(cumChars/totalChars)
        L.append(logLikelihood)
    return np.array(L)

logL = calcLikelihood(Xtrain)
# for x in L:
#     result = ','.join(map(str, x))
#     print(result)
indices = np.concatenate((np.arange(10,20), np.arange(30, 40), np.arange(50, 60)))
Ytest = Y[indices]
Xtest = X[indices]
# print(Xtest[0])

def test(X, Y, logL):
    confusion =  np.zeros((3, 3))
    for i in range(len(X)):
        dataPt = X[i]
      
        logConditionals = np.dot(dataPt, logL.T)
        posteriors = logConditionals + np.log10(PRIOR)
        posteriors = posteriors - np.max(posteriors)
        if i==0:
            print(logConditionals,posteriors)
        prediction = np.argmax(posteriors)
        label = int(i/10)
        confusion[prediction][label] += 1
    return confusion

# print(test(Xtest, Ytest, logL))

#sklearn
clf = MultinomialNB(alpha=0.5)
clf.fit(Xtrain, Ytrain)
predictions = clf.predict(Xtest)
# print(confusion_matrix(Ytest, predictions))
prior_probabilities = np.exp(clf.class_log_prior_)
# print(prior_probabilities)







def dload_MNIST():
    transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,), (0.5,))])
    mnist_train = datasets.MNIST(root='./MNIST', train=True, transform=transform, download=True)
    mnist_test = datasets.MNIST(root='./MNIST', train=False, transform=transform, download=True)
# dload_MNIST()

def sigmoid(z):
    return 1/(1+np.exp(-z))


def grad_sigmoid(z):
    sig = sigmoid(z)
    return np.matmul(sig,sig.T)


# implemented using chatGPT
def softmax(x):
    if x.ndim == 1:
        e_x = np.exp(x - np.max(x))
        return e_x / e_x.sum()
    else:
        e_x = np.exp(x - np.max(x, axis=1, keepdims=True))
        return e_x / e_x.sum(axis=1, keepdims=True)


def forward(input, w1, w2):
    hi = np.dot(input,w1)
    ho = sigmoid(hi)

    oi = np.dot(ho,w2)
    oo = softmax(oi)

    return hi, ho, oo


def train(data, labels, batch_size, epochs, layer_size, alpha):
    #w1 = np.random.randn(784, layer_size)
    #w2 = np.random.randn(layer_size, 10)
    #w1 = np.zeros((784,layer_size))
    #w2 = np.zeros((layer_size,10))
    w1 = 2 * np.random.random((784, layer_size)) - 1
    w2 = 2 * np.random.random((layer_size, 10)) - 1
    for epoch in range(epochs):
        #  iterate over minibatches
        for i in range(0, len(data), batch_size):
            batch_data = data[i:i+batch_size]
            batch_labels = labels[i:i+batch_size]
            batch_data = batch_data.reshape(batch_size,-1)
            # print(batch_data.shape)
            # print(batch_labels.shape)

            hidden_inputs, hidden_outputs, outputs = forward(batch_data, w1, w2)

            # backward
            smgradient = outputs - batch_labels
            w2gradient = np.dot(hidden_outputs.T, smgradient)
            hogradient = np.dot(smgradient,w2.T)
            higradient = np.dot(grad_sigmoid(hidden_inputs),hogradient)
            w1gradient = np.dot(batch_data.T, higradient)

            w2 -= alpha*w2gradient
            w1 -= alpha*w1gradient
    return w1,w2


mnist_train = datasets.MNIST(root='./MNIST', train=True, transform=None, download=False)
mnist_test = datasets.MNIST(root='./MNIST', train=False, transform=None, download=False)
train_data = mnist_train.data.numpy()
train_labels = mnist_train.targets.numpy()

# Normalize input pixels
train_data = train_data/255.0

# Convert labels to one-hot encoded vectors, implemented using chatGPT
encoder = LabelBinarizer()
train_labels = encoder.fit_transform(train_labels)

w1,w2 = train(train_data, train_labels, batch_size=60, epochs=150, layer_size=300, alpha=0.003)
# print(w1)
# print(w2)

def predict(data, w1, w2):
    _, _, prediction = forward(data, w1, w2)
    return np.argmax(prediction)

test_data = mnist_test.data.numpy()
test_data = test_data/255.0
test_labels = mnist_test.targets.numpy()
test_data_flat = test_data.reshape(len(test_data), -1)

# parallel testing, implemented using chatGPT
import concurrent.futures

def parallel_testing(test_data, w1, w2, num_workers):
    test_predictions = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=num_workers) as executor:
        # Use executor.map to parallelize the predictions
        test_predictions = list(executor.map(predict, test_data, [w1] * len(test_data), [w2] * len(test_data)))
    return test_predictions

num_workers = 8
test_predictions = parallel_testing(test_data_flat, w1, w2, num_workers)
test_accuracy = accuracy_score(test_labels, test_predictions)
print(test_accuracy)





# keras implementation
import keras
from keras.utils import to_categorical

(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
x_train = x_train / 255.0
x_test = x_test / 255.0

# One-hot encode the labels
y_train = to_categorical(y_train, num_classes=10)
y_test = to_categorical(y_test, num_classes=10)

model = keras.Sequential([
    keras.layers.Flatten(input_shape=(28, 28)),
    keras.layers.Dense(300, activation='sigmoid', use_bias=False),
    keras.layers.Dense(10, activation='softmax', use_bias=False)
])
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
model.fit(x_train, y_train, epochs=1, batch_size=60)


_, test_accuracy = model.evaluate(x_test, y_test, batch_size=60)
print(test_accuracy)
