import numpy as np
import matplotlib.pyplot as plt
from bisect import insort
import math
from sklearn.metrics import accuracy_score, precision_score, recall_score

# confidence, labels are assumed to be sorted in descending order of confidence
# labels assumes 1 for positive 0 for negative
def ROC(confidence, labels, name="ROC.png"):
    numP = sum(labels)
    numN = len(labels) - numP
    FP = 0.0
    TP = 0.0
    lastTP = 0.0
    FPR = [0.0]
    TPR = [0.0]
    for i in range(numP + numN):
        if((i>1) and (confidence[i] != confidence[i-1]) and (labels[i]==0) and (TP>lastTP)):
            FPR.append(FP/numN)
            TPR.append(TP/numP)
            lastTP = TP
        if (labels[i]):
            TP += 1
        else:
            FP += 1
    
    FPR.append(FP/numN)
    TPR.append(TP/numP)
    print(FPR)
    print(TPR)
    plt.figure(figsize=(10,10))
    plt.plot(FPR, TPR, marker='o')
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.title("ROC Curve")
    plt.savefig(name)

def q1_5():
    confidence = [0.95, 0.85, 0.8, 0.7, 0.55, 0.45, 0.4, 0.3, 0.2, 0.1]
    labels = [1, 1, 0, 1, 1, 0, 1, 1, 0, 0]
    ROC(confidence, labels, "q1_5.png")
    
q1_5()

def readFile(filename):
    data = []
    with open(filename, 'r') as file:
        for line in file:
            nums = line.strip().split()
            subarray = [float(num) for num in nums]
            data.append(subarray)
    return data

def calEuclidean(A, B):
    return np.linalg.norm(A-B)

def predictkNN(k, trainData, testPoint):
    kShortestDistances = []
    labelMap = {}

    for trainingPoint in trainData:
        d = calEuclidean(trainingPoint[:-1], testPoint)

        if len(kShortestDistances) < k:
            insort(kShortestDistances, d)
            if d in labelMap:
                labelMap[d].append(trainingPoint[-1])
            else:
                labelMap[d] = [trainingPoint[-1]]
        else:
            if d < kShortestDistances[-1]:
                kShortestDistances.pop()
                insort(kShortestDistances, d)
                if d in labelMap:
                    labelMap[d].append(trainingPoint[-1])
                else:
                    labelMap[d] = [trainingPoint[-1]]
    
    kNearestLabels = []
    numPos = 0.0
    for i in kShortestDistances:
        labels = labelMap[i]
        for j in labels:
            kNearestLabels.append(j)
            if j>0:
                numPos += 1
    return np.argmax(np.bincount(kNearestLabels)), (numPos/k)

def q2_1():
    train = readFile("D2z.txt")
    testX = np.arange(-2, 2.1, 0.1)
    testY = np.arange(-2, 2.1, 0.1)
    plt.figure(figsize=(10,10))

    # predict and plot labels for test data
    # red is used for label 1, blue is used for label 0
    for i in testX:
        for j in testY:
            label = predictkNN(1, train, [i,j])
            color = 'r' if label == 1.0 else 'b'
            plt.scatter(i, j, color=color, marker='o', label=label)
    
    # plot training data using triangle for label 1, square for label 0
    for point in train:
        label = point[-1]
        marker = '^' if label == 1.0 else 's'
        plt.scatter(point[0], point[1], marker=marker, label=label, edgecolor='black', facecolor='none')
    
    plt.savefig("q2_1.png")

# q2_1()

def readCSV(filename):
    data = []
    with open(filename, 'r') as file:
        file.readline() # discard first line
        for line in file:
            nums = line.strip().split(',')
            subarray = np.array([float(num) for num in nums[1:]])
            data.append(subarray)
    return np.array(data)

def calcDistanceMap(data):
    map = {}
    for i in range(len(data)):
        for j in range(len(data)):
            if (i == j):
                continue
            if (i,j) in map or (j,i) in map:
                continue
            d = calEuclidean(data[i][:-1], data[j][:-1])
            map[(i,j)] = d
    return map

def predictkNNMap(k, dMap, p, trainIndices, labels):
    kShortestDistances = []
    labelMap = {}

    for i in trainIndices:
        d = dMap[(i,p)] if (i,p) in dMap else dMap[(p,i)]

        if len(kShortestDistances) < k:
            insort(kShortestDistances, d)
            if d in labelMap:
                labelMap[d].append(labels[i])
            else:
                labelMap[d] = [labels[i]]
        else:
            if d < kShortestDistances[-1]:
                kShortestDistances.pop()
                insort(kShortestDistances, d)
                if d in labelMap:
                    labelMap[d].append(labels[i])
                else:
                    labelMap[d] = [labels[i]]

    kNearestLabels = []
    for i in kShortestDistances:
        labels = labelMap[i]
        for j in labels:
            kNearestLabels.append(j)
    return np.argmax(np.bincount(kNearestLabels))

def q2_2():
    data = readCSV("emails.csv")
    distMap = calcDistanceMap(data)
    print(f"len dMap {len(distMap)}")
    labels = np.array([arr[-1] for arr in data])
    print(len(labels))

    # train and test on 5 fold cross validation
    for i in range(5):
        a = i*1000
        testIndices = [x for x in range(a,a+1000)]
        trainIndices = [x for x in range(a)] + [x for x in range(a+1000,len(data))]

        TP = 0
        FP = 0
        TN = 0
        FN = 0        
        # iterate over testIndices
        for p in testIndices:
            predLabel = predictkNNMap(1, distMap, p, trainIndices, labels)
            label = labels[p]
            if (predLabel == 0) and (label == 0):
                TN += 1
            elif (predLabel == 0) and (label == 1):
                FN += 1
            elif (predLabel == 1) and (label == 0):
                FP += 1
            elif (predLabel == 1) and (label == 1):
                TP += 1
        
        acc = (TP+TN)/(TP+TN+FP+FN)
        recall = TP/(TP+FN)
        precision = TP/(TP+FP)
        print(f"{i+1} & {acc} & {precision} & {recall}")

# q2_2()

def q2_4():
    data = readCSV("emails.csv")
    distMap = calcDistanceMap(data)
    labels = np.array([arr[-1] for arr in data])
    ks= [1,3,5,7,10]
    avgAccs = []
    for k in ks:
        totalAcc = 0
        # train and test on 5 fold cross validation
        for i in range(5):
            a = i*1000
            testIndices = [x for x in range(a,a+1000)]
            trainIndices = [x for x in range(a)] + [x for x in range(a+1000,len(data))]

            TP = 0
            FP = 0
            TN = 0
            FN = 0        
            # iterate over testIndices
            for p in testIndices:
                predLabel = predictkNNMap(k, distMap, p, trainIndices, labels)
                label = labels[p]
                if (predLabel == 0) and (label == 0):
                    TN += 1
                elif (predLabel == 0) and (label == 1):
                    FN += 1
                elif (predLabel == 1) and (label == 0):
                    FP += 1
                elif (predLabel == 1) and (label == 1):
                    TP += 1
            
            acc = (TP+TN)/(TP+TN+FP+FN)
            totalAcc += acc

        avgAcc = totalAcc/5
        avgAccs.append(avgAcc)
    
    plt.figure(figsize=(10,10))
    plt.plot(ks, avgAccs, marker='o')
    plt.xlabel("k")
    plt.ylabel("Average accuracy")
    plt.title("kNN 5-Fold Cross validation")
    plt.savefig("q2_4.png")

# q2_4()

def updateParams(wt, b, lr, X, Y):
    # compute sigmoid
    Yhat = 1/(1 + np.exp(-(X.dot(wt) + b)))

    # compute gradients
    m = len(Y)
    dw = (1/m)*np.dot(X.T, (Yhat - Y))
    db = (1/m)*np.sum(Yhat-Y)

    # compute new params
    wnew = wt - lr*dw
    bnew = b - lr*db

    return wnew,bnew

def trainLGR(X, Y, lr, numEpochs):
    wt = np.zeros(len(X[0]))
    b = 0

    # gradient descent
    for i in range(numEpochs):
        wt,b = updateParams(wt, b, lr, X, Y)
    
    return wt,b

def predictLGR(wt, b, test):
    Yhat = 1/(1 + np.exp(-(test.dot(wt) + b)))
    return np.where(Yhat > 0.5, 1.0, 0.0), Yhat

def q2_3():
    data = readCSV("emails.csv")
    labels = data[:, -1]
    data = data[:, :-1]
    lr = 0.2
    numEpochs = 3000

    # min max normalization
    min_val = 0  # Minimum value
    max_val = 1  # Maximum value
    min_data = np.min(data, axis=0)
    max_data = np.max(data, axis=0)
    data = (data - min_data) / (max_data - min_data) * (max_val - min_val) + min_val

    for i in range(5):
        a = i*1000
        XTest = data[a:a+1000]
        YTest = labels[a:a+1000]
        XTrain = np.concatenate((data[:a], data[a+1000:]))
        YTrain = np.concatenate((labels[:a], labels[a+1000:]))
    
        wt,b = trainLGR(XTrain, YTrain, lr, numEpochs)

        #training data accuracy
        XPredTrain, _ = predictLGR(wt, b, XTrain)
        acc = accuracy_score(YTrain,XPredTrain)
        # print(f"train acc {acc}")

        # test data accuracy
        XPredTest, _ = predictLGR(wt,b,XTest)
        acc = accuracy_score(YTest, XPredTest)
        precision = precision_score(YTest, XPredTest)
        recall = recall_score(YTest, XPredTest)
        print(f"{i+1} & {acc} & {precision} & {recall}")

# q2_3()

def q2_5():
    data = readCSV("emails.csv")
    trainData = data[:4000]
    testData = data[4000:]
    YTest = testData[:,-1]

    # kNN
    confidenceArr = []
    for point in testData:
        _, confidence = predictkNN(5, trainData, point[:-1])
        confidenceArr.append(confidence)
    confidenceArr = np.array(confidenceArr)

    sortedIdxKNN = np.argsort(-confidenceArr)
    sortedConfidenceKNN = confidenceArr[sortedIdxKNN]
    sortedLabelsKNN = YTest[sortedIdxKNN]

    # plot kNN ROC
    numP = np.sum(sortedLabelsKNN)
    numN = len(sortedLabelsKNN) - numP
    total = len(YTest)
    FP = 0.0
    TP = 0.0
    lastTP = 0.0
    FPR = [0.0]
    TPR = [0.0]
    for i in range(total):
        if((i>1) and (sortedConfidenceKNN[i] != sortedConfidenceKNN[i-1]) and (sortedLabelsKNN[i]==0) and (TP>lastTP)):
            FPR.append(FP/numN)
            TPR.append(TP/numP)
            lastTP = TP
        if (sortedLabelsKNN[i]):
            TP += 1
        else:
            FP += 1
    
    FPR.append(FP/numN)
    TPR.append(TP/numP)

    plt.figure(figsize=(10,10))
    plt.plot(FPR, TPR, color='b', label='KNeighborsClassifier')
    

    # LGR
    labels = data[:, -1]
    data = data[:, :-1]
    lr = 0.2
    numEpochs = 3000

    # min max normalization
    min_val = 0  # Minimum value
    max_val = 1  # Maximum value
    min_data = np.min(data, axis=0)
    max_data = np.max(data, axis=0)
    data = (data - min_data) / (max_data - min_data) * (max_val - min_val) + min_val

    XTrain = data[:4000]
    YTrain = labels[:4000]
    wt,b = trainLGR(XTrain, YTrain, lr, numEpochs)
    XTest = data[4000:]
    _,Yhat = predictLGR(wt,b,XTest)

    sortedIdxLGR = np.argsort(-Yhat)
    sortedYhat = Yhat[sortedIdxLGR]
    sortedLabelsLGR = YTest[sortedIdxLGR]

    # plot LGR ROC
    FP = 0.0
    TP = 0.0
    lastTP = 0.0
    FPR = [0.0]
    TPR = [0.0]
    for i in range(total):
        if((i>1) and (sortedYhat[i] != sortedYhat[i-1]) and (sortedLabelsLGR[i]==0) and (TP>lastTP)):
            FPR.append(FP/numN)
            TPR.append(TP/numP)
            lastTP = TP
        if (sortedLabelsLGR[i]):
            TP += 1
        else:
            FP += 1
    
    FPR.append(FP/numN)
    TPR.append(TP/numP)

    plt.plot(FPR, TPR, color='r', label='LogisticRegression')

    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.title("ROC Curve")
    plt.grid(True)
    plt.legend()
    plt.savefig("q2_5.png")

# q2_5()
    
